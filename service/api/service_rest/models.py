from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin_number = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, unique=True, null=True)

    def __str__(self):
        return self.vin_number


class Technician(models.Model):
    technician_name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField()

    def __str__(self):
        return self.technician_name


class Service(models.Model):
    vin_number = models.ForeignKey(
        AutomobileVO,
        related_name="services",
        on_delete=models.CASCADE
    )
    customer_name = models.CharField(max_length=75)
    date = models.DateTimeField()
    time = models.TimeField()
    reason = models.TextField()
    vip = models.BooleanField(default=False)
    technician_name = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE,
    )
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.customer_name
