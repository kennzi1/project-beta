from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Service


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin_number",
        "import_href"
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "technician_name",
        "employee_number"
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin_number",
        "customer_name",
        "date",
        "time:",
        "technician_name",
        "reason",
        "completed"
    ]
    encoders = {
        "technician_name": TechnicianEncoder(),
        "vin_number": AutomobileVOEncoder()
    }

    def get_extra_data(self, o):
        return {
            "technician_name": o.technician_name,
            "vin_number": o.vin_number
        }


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()

        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)

            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except:
            response = JsonResponse(
                {"technician": "Create Technician!!"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status=400,
            )

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status=400,
            )

    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()

        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        content = {
            **content,
            "technician": Technician.objects.get(pk=content["technician"])
        }
        services = Service.objects.create(**content)

        return JsonResponse(
            services,
            encoder=ServiceEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_services(request, pk):
    if request.method == "DELETE":
        services = Service.objects.get(id=pk)
        services.delete()
        return JsonResponse(
            appointment,
            encoder=ServiceEncoder,
            safe=False,
        )
    elif request.method == "GET":
        appointment = Service.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        Service.objects.filter(id=pk).update(**content)
        services = Service.objects.get(id=pk)
        return JsonResponse(
            services,
            encoder=ServiceEncoder,
            safe=False,
        )
