from django.urls import path

from .views import (
    api_list_technician, api_detail_technician, api_list_services,  api_detail_services
)

urlpatterns = [
    path("technicians/", api_list_technician, name="api_list_technician"),
    path("technicians/<int:pk>/", api_detail_technician, name="api_detail_technician"),
    path("services/", api_list_services, name="api_list_service"),
    path("services/<int:pk>/", api_detail_services, name="api_detail_service"),
]
