import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [services, setService] = useState([])
    const [search, setSearch] = useState('')

    const handleSetSearch = e => {
        setSearch(e.target.value)
    }

    async function fetchService() {
        const res = await fetch('http://localhost:8080/api/services');
        const getData = await res.json();
        const serviceArray = getData.services
        const result = serviceArray.filter(item => item.vin.vin === search)
        setService(result)
    }

    useEffect(() => { fetchService() }, [])


    return (
        <>
            <br></br>
            <div className="input-group">
                <input type="text" value={search} onChange={handleSetSearch} className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                <button type="button" onClick={fetchService} className="btn btn-outline-secondary">Search VIN</button>
            </div>

            <h1>Service</h1>
            <table className="table table-striped">
                <thead>

                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>

                    {services.map((service) => {
                        return (
                            <tr>
                                <td>{service.vin.number}</td>
                                <td>{service.owner}</td>
                                <td>{service.date}</td>
                                <td>{service.time}</td>
                                <td>{service.technician.name}</td>
                                <td>{service.reason}</td>
                            </tr>
                        )
                    })}

                </tbody>
            </table>
        </>
    )
}

export default ServiceHistory

