import React, { useState, useEffect } from 'react';

function ServiceList() {
    const [services, setServices] = useState([]);
    async function fetchService() {
        const res = await fetch('http://localhost:8080/api/services');
        const newService = await res.json();
        setServices(newService.services.filter(record => record.completed !== true))
    }

    useEffect(() => {
        fetchService()
    }, [])

    const completed = async (id) => {
        const url = 'http://localhost:8080/api/services/'
        const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify({ status: 2 }),
            headers: { 'Content-Type': 'application/json', }

        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            fetchService()
        }
    }
    const canceled= async (id) => {
        const url = 'http://localhost:8080/api/services/'
        const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify({ status: 2 }),
            headers: { 'Content-Type': 'application/json', }

        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            fetchService()
        }
    }


    return (
        <>
            <h1>Service</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN Number</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Completed</th>
                    </tr>
                </thead>
                <tbody>
                    {services.map((service) => {
                        return (
                            <tr>
                                <th>{service.id}</th>
                                <td>{service.vin}</td>
                                <td>{service.customer_name}</td>
                                <td>{service.date}</td>
                                <td>{service.time}</td>
                                <td>{service.technician.name}</td>
                                <td>{service.reason}</td>
                                <td>{service.vip}</td>
                                <td>{service.completed}</td>
                                <td><button onClick={e => canceled(e, service.id)} id={service.id} type="button" className="btn btn-danger rounded-0">Cancel</button></td>
                                <td><button onClick={() => completed(service.id)} id={service.id} type="button" className="btn btn-success rounded-0">Completed</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ServiceList;

