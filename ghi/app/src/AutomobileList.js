import React from "react";

export default function AutomobileList ({ autos }) {
    console.log(autos)
    return (
        <>
            <h1 className="display-4 fw-bold text-center">Automobiles</h1>
            <table className="table .table-bordered table-striped table-success text-left">
                <thead>
                    <tr>
                        <th >Vin Number</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {autos && autos.map(automobile => {
                        return (
                            <tr key={automobile.vin}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
