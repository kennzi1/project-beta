# CarCar

CarCar is a web based application used to manage an automobile dealership. The application supports inventory management, sales records, and service records as separate micro-services:

- Inventory management
    - Show and create vehicle manufacturers
    - Show and create vehicle models
    - Show and create individual automobiles in the current inventory
- Sales Records
    - Add sales people
    - Add potential customers
    - Create sale records
    - List all sales
    - List sales filtered by sales person
- Service Records
    - Add service technician
    - Enter service appointments
    - List all service appointments
    - List service history by automobile

## Install the app:

From [gitlab.com/kennzi1/project-beta](https://gitlab.com/kennzi1/project-beta):

1. Fork the repo to your gitLab namespace

![fork](screenshots/fork.png)

2. Clone the repo from your fork using HTTPS

![clone](screenshots/clone.png)

3. In your terminal, access the directory where you want the project to reside

```bash
yourName$ cd myProjects
```

4. Clone the project

```bash
yourDirectory yourName$ git clone <paste from gitlab here> .
```

5. Change your directory to the project directory

```bash
cd <project name>
```

6. Upgrade pip

```bash
pip install --upgrade pip
```

7. Install the requirements from requirements.txt

```bash
pip install -r requirements.txt
```

8. Open Docker desktop app
9. Build the docker services

```bash
docker-compose build
```

10. Start all docker containers:

```bash
docker-compose up
```

## View the app in your browser at localhost:3000

With the app up and running you will see the main page with dropdown links in the top Navigation bar to Inventory, Sales, and Services.

![Main Page](screenshots/MainPage.png)

Click the “down” arrow next to each link to view the related pages.

## Sales Micro-service by McKenzie Barker

### Back-end:

The models created for the back-end of this micro-service include:

Customer with name, address, and phone number properties

SalesPerson with name and employee number properties

Automobile Value Object to represent each instance of the Automobile model from the inventory micro-service. These value objects are polled from the inventory RESTful API at port 8100

SaleRecord with foreign key properties to the customer model, salesPerson model, and automobileVO model and an additional price property

There are views for listing, adding, deleting, and updating the models listed above.

### Front-end:

There are links under the “Sales” dropdown from the main page for each of the following pages:

New Customer - a form to add a customer

New Employee - a form to add a sales person

New Sales Record - a form to record a new sale

All Sales Records - a list of every sale

Sales by employee - a list of every sale for a specific sales person that is selected from a dropdown menu at the top of the list

## Services Micro-service by Tommy Mai

### Back-end:

The models created for the back-end of this micro-service include:

Automobile Value Object to represent each instance of the Automobile model from the inventory micro-service. These value objects are polled from the inventory RESTful API at port 8100

Technician with technician name and employee number properties

Service with vin number, customer name, date, time, reason, completed if it is vip or not, foreign key of technician name.

There are views fo listing, adding, deleting, and updating the models listed above.

### Front-end:

There are links under the “Services” dropdown from the main page for each of the following pages:

Create Service - a form to create a service

Service Appointments - a list of service appointments

Service History - A list of every service appointment

New Technician - a form to add a technician


## Inventory Micro-service

### Back-end:

The models created for the back-end of this micro-service include:

Manufacturer with a name property

Vehicle Model with a name and picture url property, and a foreign key property to the manufacturer

Automobile with color, year, and vin properties. It also includes a foreign key property to the vehicle model.

### Front-end:

There are links under the “Sales” dropdown from the main page for each of the following pages:

Manufacturers - a list of all manufacturers 

Models - a list of all models

Automobiles - a list of all automobiles in the inventory

Add a manufacturer - a form to add a vehicle manufacturer

Add a model - a form to add a vehicle model

Add an automobile - a form to create a new automobile in the inventory

##
